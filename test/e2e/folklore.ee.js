var config = require('../../nightwatch.conf.js');

module.exports = {
  'folklore': function(browser) {
    browser
      .url('https://www.folklore.ee/')
      .waitForElementVisible('body')
      .verify.title('Eesti Rahvaluule :: Indeks')
      //.assert.title('Eesti Rahvaluule :: Indeks')
      .waitForElementVisible('body')
      .assert.containsText("body", "Andmebaasid")
      .saveScreenshot(config.imgpath(browser) + 'flk_001avaleht.png')
      .useXpath()
      .click("//a[text()='Andmebaasid']")
      //.click('select[id="menu"] option[value="3"]')
      .useCss()

      .waitForElementVisible('body')
      .verify.containsText('body', 'Andmebaasid')
      .assert.containsText('body', 'Netihuumor')
      .saveScreenshot(config.imgpath(browser) + 'flk_002baasid.png')
      .useXpath()
      .click("//a[text()='Netihuumor.']")
      .useCss()

      .waitForElementVisible('body')
      .assert.visible('input[name="q"]')
      //.saveScreenshot(config.imgpath(browser) + 'flk_003paring.png')
      //.pause(4000)
      //.useXpath()
      //.waitForElementVisible('input[name="q"]', 6000)
      //input:nth-of-type(3)
      .setValue('input:nth-of-type(3)', 'kodune töö')
      //.setValue('input[name="q"]', "kodune töö")
      //.setValue('input[name="q"]', "kodune töö")
      .click('input:nth-of-type(7)')
      //.click('select[type="radio"] option[value=""]')
      .saveScreenshot(config.imgpath(browser) + 'flk_003paring.png')
      .click('input[value="Anekdoodiotsing"]')
      //.useCss()

      .waitForElementVisible('body')
      .verify.containsText('body', 'kodune')
      .saveScreenshot(config.imgpath(browser) + 'flk_004vastus.png')

      //ja mängime veel suurustega
      .windowSize('current', 1000, 100)
      .saveScreenshot(config.imgpath(browser) + 'flk_005txs.png')
      .windowSize('current', 100, 1000)
      .saveScreenshot(config.imgpath(browser) + 'flk_006sxt.png')


      .pause(1000)

      .end();
  }
};
